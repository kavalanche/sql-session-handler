<?php

namespace Kavalanche\SqlSessionHandler;

use PDO;
use SessionHandlerInterface;
use function Kavalanche\SqlSessionHandler\config;

class SqlSessionHandler implements SessionHandlerInterface {

    private PDO $db;
    private string $tableName;

    public function __construct(PDO $db) {
        $this->db = $db;
        $this->tableName = config('session-table-name');
    }

    public function close(): bool {
        return true;
    }

    public function destroy(string $id): bool {
        $sql = "DELETE FROM " . $this->tableName . " WHERE id = :id";
        $stmt = $this->db->prepare($sql);
        return $stmt->execute([
                    'id' => $id
        ]);
    }

    public function gc(int $max_lifetime): int|false {
        $sql = "DELETE FROM " . $this->tableName . " WHERE timestamp <= :timestamp";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([
            'timestamp' => time() - $max_lifetime
        ]);
        return $stmt->rowCount();
    }

    public function open(string $path, string $name): bool {
        return true;
    }

    public function read(string $id): string|false {
        $sql = "SELECT data FROM " . $this->tableName . " WHERE id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([
            'id' => $id
        ]);

        $data = $stmt->fetch();

        return !empty($data) ? $data['data'] : '';
    }

    public function write(string $id, string $data): bool {
        /**
         * Cannot use $this->read($id) because of php internal problem when open returns false
         * Hence the ugly hack
         */
        $sql = "SELECT data FROM " . $this->tableName . " WHERE id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([
            'id' => $id
        ]);
        if ($stmt->fetch() === false) {
            $sql = "INSERT INTO " . $this->tableName . " (id, data, timestamp) VALUES (:id, :data, :timestamp)";
            $stmt = $this->db->prepare($sql);
            return $stmt->execute([
                        'id' => $id,
                        'data' => $data,
                        'timestamp' => time()
            ]);
        }

        $sql = "UPDATE " . $this->tableName . " SET data = :data, timestamp = :timestamp WHERE id = :id";
        $stmt = $this->db->prepare($sql);
        return $stmt->execute([
                    'id' => $id,
                    'data' => $data,
                    'timestamp' => time()
        ]);
    }
}
