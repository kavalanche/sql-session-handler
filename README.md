# Kavalanche/SqlSessionHandler
SQL session handler for PHP

## Usage

1. Require `kavalanche/sql-session-handler`.

    ```bash
    composer require kavalanche/sql-session-handler
    ```

2. Create instance of `Kavalanche\SqlSessionHandler\SqlSessionHandler` and inject your database handle (PDO) into it.

    ```php
    $SqlSessionHandler = new Kavalanche\SqlSessionHandler\SqlSessionHandler($db);
    session_set_save_handler($SqlSessionHandler);
    session_start();
    ```

## Database structure

```sql
CREATE TABLE `session` (
  `id` char(32) CHARACTER SET ascii NOT NULL,
  `timestamp` int(10) unsigned NOT NULL,
  `data` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
```

## Configuration

You can change session table name in `config/sql-session-handler.yaml` placed in the root of your application.

Default value is:
```yaml
session-table-name: session
```